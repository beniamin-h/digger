'use strict';

angular.module('digger.map', ['ui.bootstrap'])

.value('mapConfig', {
    mapDim: {
      x: 20,
      y: 80
    },
    blockSize: 50,
    mapWindowDim: {
      x: 1000,
      y: 800
    },
    mapOffsetTop: 100,
    drillerBasePosition: {
      x: 10,
      y: -1
    },
    scrollBuffer: 4
  })

.controller('MapCtrl', ['$scope', '$rootScope', 'mapProvider', 'mapConfig', '$window', 'keyCodes', 'drillerProvider',
    function($scope, $rootScope, mapProvider, mapConfig, $window, keyCodes, drillerProvider) {

  $scope.mapScrollOffset = { top: 0 };
  $scope.driller = { top: 50, left: 500 };

  var setDrillerPosition = function (newPosition) {
    $scope.driller.top = newPosition.y * mapConfig.blockSize + mapConfig.mapOffsetTop;
    $scope.driller.left = newPosition.x * mapConfig.blockSize;
  };

  var scrollMap = function (drillerPosition) {
    var scrollBufferPx = mapConfig.scrollBuffer * mapConfig.blockSize;
    var maxYPosition = mapConfig.mapWindowDim.y - mapConfig.mapOffsetTop - scrollBufferPx;
    var minYPosition = mapConfig.scrollBuffer * mapConfig.blockSize;
    var drillerYPosition = mapConfig.mapOffsetTop + drillerPosition.y * mapConfig.blockSize;

    if (drillerYPosition + $scope.mapScrollOffset.top > maxYPosition) {
      $scope.mapScrollOffset.top = -(drillerYPosition - maxYPosition);
    }
    if (drillerYPosition + $scope.mapScrollOffset.top < minYPosition && drillerYPosition >= minYPosition) {
      $scope.mapScrollOffset.top = -(drillerYPosition - minYPosition);
    }
  };

  // ------------------- INIT -------------------

  $scope.initMap = function() {
    $scope.blocks = mapProvider.getAllBlocks();
    setDrillerPosition(mapConfig.drillerBasePosition);
    drillerProvider.setPosition(mapConfig.drillerBasePosition);
  };

  // ------------------- LISTENERS -------------------

  angular.element($window).on('keydown', function(e) {
    var key = keyCodes.toCode(e.keyCode);
    if (['down', 'up', 'right', 'left'].indexOf(key) > -1) {
      var newPosition = drillerProvider.move(key);
      if (newPosition) {
        setDrillerPosition(newPosition);
        scrollMap(newPosition);
        $scope.$apply();
      }
      e.preventDefault();
    }
  });

  // ------------------- SCOPE METHODS -------------------



}]);
