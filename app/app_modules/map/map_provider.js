'use strict';

angular.module('digger.map').factory('mapProvider', ['mapConfig', 'blocksProvider', 'Math',
    function (mapConfig, blocksProvider, Math) {

  var that = this;

  return {
    getWidth: function () {
      return mapConfig.mapDim.x;
    },
    getHeight: function () {
      return mapConfig.mapDim.y;
    },
    initMap: function () {
      for (var i = 0; i < this.getWidth() * this.getHeight(); i++) {
        blocksProvider.createBlock(i, i % mapConfig.mapDim.x, Math.floor(i / mapConfig.mapDim.x));
      }
    },
    getAllBlocks: function () {
      return blocksProvider.getAllBlocks();
    },
    isPositionValid: function (position) {
      if (position.x < 0) {
        return false;
      }
      if (position.y < -1) {
        return false;
      }
      if (position.x >= mapConfig.mapDim.x) {
        return false;
      }
      if (position.y >= mapConfig.mapDim.y) {
        return false;
      }

      return true;
    },
    _instance: this
  };
}]);