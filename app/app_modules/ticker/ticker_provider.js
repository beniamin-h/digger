'use strict';

angular.module('digger.ticker').factory('tickerProvider', ['Ticker',
    function (Ticker) {

  var that = this;

  if (!this.ticker) {
    this.ticker = new Ticker();
  }

  return {
    tick: function () {
      that.ticker.tick();
    },
    now: function () {
      return that.ticker.nowTickStamp();
    },
    _instance: this.ticker
  };
}]);
