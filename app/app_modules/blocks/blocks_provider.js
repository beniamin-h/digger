'use strict';

angular.module('digger.blocks', []).factory('blocksProvider', ['Block', 'mapConfig',
    function (Block, mapConfig) {

  var that = this;

  this.blocks = [];

  return {
    createBlock: function (i, x, y) {
      var block = new Block(i, x, y);
      that.blocks.push(block);
      return block;
    },
    getAllBlocks: function () {
      return that.blocks;
    },
    getBlockByXY: function (x, y) {
      return that.blocks[y * mapConfig.mapDim.x + x];
    },
    getBlockIndexByXY: function (x, y) {
      return y * mapConfig.mapDim.x + x;
    },
    _instance: this
  };
}]);
