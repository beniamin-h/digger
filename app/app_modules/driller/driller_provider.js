'use strict';

angular.module('digger.driller', []).factory('drillerProvider', ['Driller', 'Cargo',
    function (Driller, Cargo) {

  if (!this.driller) {
    this.driller = new Driller(new Cargo());
  }

  return this.driller;
}]);
