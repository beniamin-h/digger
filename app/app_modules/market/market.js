'use strict';

angular.module('digger.market', []).controller('MarketCtrl', ['$scope', 'LocalMarket', function($scope, LocalMarket) {

  $scope.transactionAmount = 0;
  $scope.maxTransactionAmount = 0;
  $scope.unitPrice = 0;
  $scope.yourMoney = 0;
  $scope.tradableResources = LocalMarket.tradableResources;
  $scope.selectedResource = null;
  $scope.transactionPanelHidden = true;
  $scope.searchText = null;

  $scope.activeTransactionTabIdx = 0;
  $scope.transactionTypes = ['Buy', 'Sell'];

  var activeTransactionType;

  var updateTransactionPanel = function () {
    if (activeTransactionType == 'Buy') {
      $scope.maxTransactionAmount = $scope.selectedResource.amountsAvailableOnLocalMarket[0];
      $scope.unitPrice = $scope.selectedResource.buyPrice;
    } else {
      $scope.maxTransactionAmount = $scope.selectedResource.amountsLocalMarketNeeds[0];
      $scope.unitPrice = $scope.selectedResource.sellPrice;
    }
    $scope.transactionAmount = 0;
  };

  // ------------------- INIT -------------------

  $scope.initMarket = function() {
    $scope.yourMoney = 10000;
  };

  // ------------------- LISTENERS -------------------

  // ------------------- SCOPE METHODS -------------------

  $scope.searchResource = function (a) {
    return !$scope.searchText || a.resName.indexOf($scope.searchText) > -1;
  };

  $scope.increaseTransactionAmount = function (amount) {
    if ($scope.transactionAmount + amount > $scope.maxTransactionAmount) {
      $scope.transactionAmount = $scope.maxTransactionAmount;
    } else {
      $scope.transactionAmount += amount;
    }
  };

  $scope.setTransactionAmount = function (amount) {
    if (!isNaN(amount)) {
      if (amount < $scope.maxTransactionAmount) {
        $scope.transactionAmount = amount;
      } else {
        $scope.transactionAmount = $scope.maxTransactionAmount;
      }
    }
  };

  $scope.updateActiveTransactionTabIdx = function (idx) {
    $scope.activeTransactionTabIdx = idx;
    activeTransactionType = $scope.transactionTypes[$scope.activeTransactionTabIdx];
    if ($scope.selectedResource) {
      updateTransactionPanel();
    }
  };

  $scope.selectResource = function (resource) {
    $scope.selectedResource = resource;
    $scope.transactionPanelHidden = false;
    updateTransactionPanel();
  };

}]).filter('round4', function () {
  return function (value) {
    return Math.round(value * 10000) / 10000.0;
  };
});
