'use strict';

angular.module('digger.market').factory('LocalMarket', ['Market', function (Market) {

  if (!this.market) {
    this.market = new Market();
  }

  return this.market;
}]);
