'use strict';

angular.module('digger.init', [])

.controller('InitCtrl', ['initProvider', function(initProvider) {

  initProvider.initGenericProviders();
  initProvider.setupTickProcessInterval();

}]);