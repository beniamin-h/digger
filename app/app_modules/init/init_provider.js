'use strict';

angular.module('digger.init').factory('initProvider', ['mapProvider', '$interval', 'tickerProvider', 'LocalMarket',
    function(mapProvider, $interval, tickerProvider, LocalMarket) {
  var that = this;

  return {
    initGenericProviders: function () {
      mapProvider.initMap();
    },
    setupTickProcessInterval: function () {
      var self = this;
      $interval(function () {
        //var now = new Date();
        self.processTicks();
        //console.log(new Date() - now);
      }, 1000);
    },
    processTicks: function () {
      tickerProvider.tick();
      LocalMarket.tick();
    },
    _instance: this
  };
}]);