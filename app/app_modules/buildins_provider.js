'use strict';

angular.module('digger.commons', [])

.factory('Math', function () {
  Math.log2 = Math.log2 || function (value) { return Math.log(value) / Math.LN2; };
  return Math;
})
.factory('Date', function () {
  return Date;
});