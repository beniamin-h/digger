'use strict';

// Declare app level module which depends on views, and components
angular.module('digger', [
  'ngRoute',
  'digger.layout',
  'digger.map',
  'digger.commons',
  'digger.blocks',
  'digger.ticker',
  'digger.init',
  'digger.resources',
  'digger.driller',
  'digger.market'
]);