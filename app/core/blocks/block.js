
angular.module('digger.blocks').factory('Block', ['Math', 'ArrayUtils', 'Coal', 'Iron', 'Dirt', 'Sandstone', 'Copper', 'Aluminium', 'Bauxite',
    function (Math, ArrayUtils, Coal, Iron, Dirt, Sandstone, Copper, Aluminium, Bauxite) {

  var Block = function (i, x, y) {
    this.index = i;
    this.x = x;
    this.y = y;
    this.resource = this.generateResource(y);
    this.content = this.generateContent(y);
  };

  Block.prototype.index = 0;
  Block.prototype.x = 0;
  Block.prototype.y = 0;
  Block.prototype.resource = null;
  Block.prototype.content = null;

  var availableResources = [
    Coal, Iron, Copper, Aluminium, Bauxite
  ];

  var availableContents = [
    Dirt, Sandstone
  ];

  var resourceGenerator = {
    generate: function (depth, resource) {
      var resRange = resource.maxDepth - resource.minDepth;
      var distToAvg = this.getDistanceToAvg(resource.minDepth, resource.maxDepth, depth);
      if (this.checkDepthLevels(resource.minDepth, resource.maxDepth, depth) &&
          this.checkDepthProbability(resRange, distToAvg, depth) &&
          this.checkResFrequency(resource.frequency)) {
        return new resource(this.calculateQuantity(
          resource.avgQuantity, resource.maxDepth, resRange, distToAvg, depth));
      } else {
        return null;
      }
    },
    checkDepthLevels: function (min, max, current) {
      return current >= min && current <= max;
    },
    checkDepthProbability: function (resRange, distToAvg, current) {
      return Math.random() < (1 - 1 / Math.log2(current + current * (resRange / 2 - distToAvg))) / 10;
    },
    getDistanceToAvg: function (min, max, current) {
      var avg = (min + max) / 2;
      return Math.abs(avg - current);
    },
    checkResFrequency: function (frequency) {
      return Math.random() < frequency;
    },
    calculateQuantity: function (avgQuantity, maxDepth, resRange, distToAvg, depth) {
      return (10 + depth) / maxDepth *  // range: (0.1, 1.1)
        Math.pow((10 + resRange - distToAvg * 2) / resRange, 1.5) *  // (0.03, 1.15)
        Math.pow(avgQuantity, 2) *  // (0, 10000)
        Math.random() * 10;  // (0, 10)
      // range: (0, 126500), med: 3450, 10th percentile: 2.96, 25th percentile: 109
    }
  };

  Block.prototype.generateResource = function (depth) {
    var generatedResource = null;

    for (var i = 0; i < availableResources.length && generatedResource === null; i++) {
      generatedResource = resourceGenerator.generate(depth, availableResources[i]);
    }

    return generatedResource;
  };

  Block.prototype.generateContent = function (depth) {

    var possibleContents = [];

    availableContents.forEach(function (content) {
      if (depth >= content.minDepth && depth <= content.maxDepth) {
        possibleContents.push(content);
      }
    });

    if (possibleContents.length === 0) {
      throw new Error('Invalid block contents configuration.');
    }

    if (possibleContents.length === 1) {
      var chosenContent = possibleContents[0];
    } else {
      var distancesToAvg = possibleContents.map(function (content) {
        return Math.abs(depth - content.avgDepth) + 1;
      });

      var totalDistanceToAvg = ArrayUtils.sum(distancesToAvg);

      var invertedDistancesToAvg = distancesToAvg.map(function (dist) {
        return totalDistanceToAvg - dist;
      });

      var totalInvertedDistancesToAvg = ArrayUtils.sum(invertedDistancesToAvg);

      var seed = Math.random() * 0.9999999;  // 1/7 + 1/7 + 1/7 + 1/7 + 1/7 + 1/7 + 1/7 < 1
      var tmpSum = 0;
      var i = 0;

      while (tmpSum < seed) {
        var chosenContent = possibleContents[i];
        tmpSum += invertedDistancesToAvg[i++] / totalInvertedDistancesToAvg;
      }
    }

    return new chosenContent(Math.random() * chosenContent.avgQuantity);
  };

  Block.prototype.isEmpty = function () {
    return !this.content;
  };

  Block.prototype.empty = function () {
    this.content = null;
    this.resource = null;
  };

  return Block;
}]);
