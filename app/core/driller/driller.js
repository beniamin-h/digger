'use strict';

angular.module('digger.driller').factory('Driller', ['mapProvider', 'blocksProvider', function (mapProvider, blocksProvider) {

  var Driller = function (cargo) {
    this.cargo = cargo;
  };

  Driller.prototype.position = {x: 0, y: 0};
  Driller.prototype.isMoving = false;
  Driller.prototype.cargo = null;

  Driller.prototype.move = function (direction) {
    var newPosition = {x: this.position.x, y: this.position.y};
    if (direction == 'left') {
      newPosition.x -= 1;
    }
    if (direction == 'right') {
      newPosition.x += 1;
    }
    if (direction == 'up') {
      newPosition.y -= 1;
    }
    if (direction == 'down') {
      newPosition.y += 1;
    }

    if (!mapProvider.isPositionValid(newPosition)) {
      return false;
    }

    if (newPosition.y >= 0) {
      var block = blocksProvider.getBlockByXY(newPosition.x, newPosition.y);

      if (!block.isEmpty()) {
        if (this.cargo.load(block.content, block.resource)) {
          block.empty();
        } else {
          return false;
        }
      }
    }

    this.position.x = newPosition.x;
    this.position.y = newPosition.y;

    return newPosition;
  };

  Driller.prototype.setPosition = function (newPosition) {
    this.position.x = newPosition.x;
    this.position.y = newPosition.y;
  };

  return Driller;
}]);