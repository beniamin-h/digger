'use strict';

angular.module('digger.driller').factory('Cargo', ['Coal', 'Iron', 'Dirt', 'Sandstone', 'Copper', 'Aluminium', 'Bauxite',
    function (Coal, Iron, Dirt, Sandstone, Copper, Aluminium, Bauxite) {

  var loadableResources = [
    Coal, Iron, Copper, Aluminium, Bauxite
  ];

  var loadableContents = [
    Dirt, Sandstone
  ];

  var Cargo = function () {
    this.capacity = 100000;
    this.loaded = {};
    var that = this;
    loadableResources.forEach(function (resClass) {
      var resource = new resClass(0);
      that.loaded[resource.resName] = resource;
    });
    loadableContents.forEach(function (resClass) {
      var resource = new resClass(0);
      that.loaded[resource.resName] = resource;
    });
  };

  Cargo.prototype.load = function (blockContent, blockResource) {
    var newLoadedWeight = this._sumLoadedWeight() + blockContent.quantity + (blockResource ? blockResource.quantity : 0);
    if (newLoadedWeight < this.capacity) {
      this.loaded[blockContent.resName].quantity += blockContent.quantity;
      if (blockResource) {
        this.loaded[blockResource.resName].quantity += blockResource.quantity;
      }
      return true;
    }

    return false;
  };

  Cargo.prototype._sumLoadedWeight = function () {
    var weight = 0.0;
    for (var resName in this.loaded) {
      weight += this.loaded[resName].quantity;
    }
    return weight;
  };

  return Cargo;
}]);