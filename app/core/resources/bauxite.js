
angular.module('digger.resources').factory('Bauxite', ['Resource',
    function (Resource) {

  var Bauxite = function () {
    Resource.apply(this, arguments);
  };

  Bauxite.prototype = Object.create(Resource.prototype);
  Bauxite.prototype.constructor = Bauxite;

  Bauxite.prototype.resName = Bauxite.resName = 'Bauxite';
  Bauxite.prototype.mapColor = Bauxite.mapColor = '#ADB2BD';

  Bauxite.minDepth = 2;
  Bauxite.maxDepth = 60;
  Bauxite.frequency = 0.5;
  Bauxite.avgQuantity = 20.0;

  Bauxite.basePrice = 20.0;
  Bauxite.buyPrice = 0.0;
  Bauxite.sellPrice = 0.0;
  Bauxite.resType = 'Ore';
  Bauxite.amountsAvailableOnLocalMarket = [];
  Bauxite.amountsLocalMarketNeeds = [];
  Bauxite.baseLocalMarketAmount = 400;

  return Bauxite;
}]);
