
angular.module('digger.resources').factory('Aluminium', ['Resource',
    function (Resource) {

  var Aluminium = function () {
    Resource.apply(this, arguments);
  };

  Aluminium.prototype = Object.create(Resource.prototype);
  Aluminium.prototype.constructor = Aluminium;

  Aluminium.prototype.resName = Aluminium.resName = 'Aluminium';
  Aluminium.prototype.mapColor = Aluminium.mapColor = '#ADB2BD';

  Aluminium.minDepth = 2;
  Aluminium.maxDepth = 60;
  Aluminium.frequency = 0.5;
  Aluminium.avgQuantity = 20.0;

  Aluminium.basePrice = 20.0;
  Aluminium.buyPrice = 0.0;
  Aluminium.sellPrice = 0.0;
  Aluminium.resType = 'Metal';
  Aluminium.amountsAvailableOnLocalMarket = [];
  Aluminium.amountsLocalMarketNeeds = [];
  Aluminium.baseLocalMarketAmount = 400;

  return Aluminium;
}]);
