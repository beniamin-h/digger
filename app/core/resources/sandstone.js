
angular.module('digger.resources').factory('Sandstone', ['Resource',
    function (Resource) {

  var Sandstone = function () {
    Resource.apply(this, arguments);
  };

  Sandstone.prototype = Object.create(Resource.prototype);
  Sandstone.prototype.constructor = Sandstone;

  Sandstone.prototype.resName = Sandstone.resName = 'Sandstone';
  Sandstone.prototype.mapColor = Sandstone.mapColor = '#cea40e';

  Sandstone.minDepth = 0;
  Sandstone.maxDepth = 200;
  Sandstone.avgDepth = 50;
  Sandstone.avgQuantity = 80.0;

  Sandstone.basePrice = 1.0;
  Sandstone.buyPrice = 0.0;
  Sandstone.sellPrice = 0.0;
  Sandstone.resType = 'Mineral';
  Sandstone.amountsAvailableOnLocalMarket = [];
  Sandstone.amountsLocalMarketNeeds = [];
  Sandstone.baseLocalMarketAmount = 10000;

  return Sandstone;
}]);
