
angular.module('digger.resources').factory('Coal', ['Resource',
    function (Resource) {

  var Coal = function () {
    Resource.apply(this, arguments);
  };

  Coal.prototype = Object.create(Resource.prototype);
  Coal.prototype.constructor = Coal;

  Coal.prototype.resName = Coal.resName = 'Coal';
  Coal.prototype.mapColor = Coal.mapColor = '#000';

  Coal.minDepth = 2;
  Coal.maxDepth = 60;
  Coal.frequency = 0.5;
  Coal.avgQuantity = 20.0;

  Coal.basePrice = 5.0;
  Coal.buyPrice = 0.0;
  Coal.sellPrice = 0.0;
  Coal.resType = 'Fuel';
  Coal.amountsAvailableOnLocalMarket = [];
  Coal.amountsLocalMarketNeeds = [];
  Coal.baseLocalMarketAmount = 1000;

  return Coal;
}]);
