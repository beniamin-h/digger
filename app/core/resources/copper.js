
angular.module('digger.resources').factory('Copper', ['Resource',
    function (Resource) {

  var Copper = function () {
    Resource.apply(this, arguments);
  };

  Copper.prototype = Object.create(Resource.prototype);
  Copper.prototype.constructor = Copper;

  Copper.prototype.resName = Copper.resName = 'Copper';
  Copper.prototype.mapColor = Copper.mapColor = '#C87533';

  Copper.minDepth = 2;
  Copper.maxDepth = 60;
  Copper.frequency = 0.5;
  Copper.avgQuantity = 20.0;

  Copper.basePrice = 10.0;
  Copper.buyPrice = 0.0;
  Copper.sellPrice = 0.0;
  Copper.resType = 'Metal';
  Copper.amountsAvailableOnLocalMarket = [];
  Copper.amountsLocalMarketNeeds = [];
  Copper.baseLocalMarketAmount = 1000;

  return Copper;
}]);
