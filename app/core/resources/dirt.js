
angular.module('digger.resources').factory('Dirt', ['Resource',
    function (Resource) {

  var Dirt = function () {
    Resource.apply(this, arguments);
  };

  Dirt.prototype = Object.create(Resource.prototype);
  Dirt.prototype.constructor = Dirt;

  Dirt.prototype.resName = Dirt.resName = 'Dirt';
  Dirt.prototype.mapColor = Dirt.mapColor = '#210';

  Dirt.minDepth = 0;
  Dirt.maxDepth = 40;
  Dirt.avgDepth = 10;
  Dirt.avgQuantity = 100.0;

  Dirt.basePrice = 0.001;
  Dirt.resType = 'Mineral';
  Dirt.amountsAvailableOnLocalMarket = [];
  Dirt.amountsLocalMarketNeeds = [];
  Dirt.baseLocalMarketAmount = 100000;

  return Dirt;
}]);
