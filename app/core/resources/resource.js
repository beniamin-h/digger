
angular.module('digger.resources', []).factory('Resource', ['Math', function (Math) {

  var Resource = function (quantity) {
    this.quantity = quantity || 0;
  };

  Resource.prototype.resName = Resource.resName = '';
  Resource.prototype.mapColor = Resource.mapColor = '';
  Resource.prototype.quantity = 0;

  Resource.minDepth = 0;
  Resource.maxDepth = 0;
  Resource.avgDepth = 0;
  Resource.frequency = 0.0;
  Resource.avgQuantity = 0.0; // 0 - 100

  Resource.basePrice = 0.0;
  Resource.buyPrice = 0.0;
  Resource.sellPrice = 0.0;
  Resource.resType = ''; // Mineral|Fuel|Metal
  Resource.amountsAvailableOnLocalMarket = []; // eg. 1, 10, 100, 10000, 10**8
  Resource.amountsLocalMarketNeeds = []; // eg. 1, 10, 100, 10000, 10**8
  Resource.baseLocalMarketAmount = 0; // 0 - 10**6

  return Resource;
}]);
