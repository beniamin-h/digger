
angular.module('digger.resources').factory('Iron', ['Resource',
    function (Resource) {

  var Iron = function () {
    Resource.apply(this, arguments);
  };

  Iron.prototype = Object.create(Resource.prototype);
  Iron.prototype.constructor = Iron;

  Iron.prototype.resName = Iron.resName = 'Iron';
  Iron.prototype.mapColor = Iron.mapColor = '#666';

  Iron.minDepth = 25;
  Iron.maxDepth = 75;
  Iron.frequency = 0.3;
  Iron.avgQuantity = 10.0;

  Iron.basePrice = 15.0;
  Iron.buyPrice = 0.0;
  Iron.sellPrice = 0.0;
  Iron.resType = 'Metal';
  Iron.amountsAvailableOnLocalMarket = [];
  Iron.amountsLocalMarketNeeds = [];
  Iron.baseLocalMarketAmount = 500;

  return Iron;
}]);
