'use strict';

angular.module('digger.market').factory('Market', ['Math', 'tickerProvider', 'Coal', 'Iron', 'Dirt', 'Sandstone', 'Copper', 'Aluminium', 'Bauxite',
    function (Math, tickerProvider, Coal, Iron, Dirt, Sandstone, Copper, Aluminium, Bauxite) {

  var Market = function () {
    this.setupInitialRecentAmountsSold();
    this.setupInitialMarketShares();
    this.setupInitialPrices();
  };

  Market.prototype.tradableResources = [
    Coal, Iron, Dirt, Sandstone, Copper, Aluminium, Bauxite
  ];

  Market.prototype.sellToMarketPriceFactors = { // sum: 6.0
    yourMarketShare: 1.0, // you can impose prices
    transactionAmount: 0.5, // more is better
    localBusinesses: 2.0, // factories, mines etc.
    randomEvents: 1.0, // natural disasters etc.
    additionalFeatures: 0.5,  //
    neededAmounts: 1.0  // best offers, average offers, ...
  };  // higher better; sell price = basePrice * factors / 6; sell to market range: (0, basePrice)
  Market.prototype.sellToMarketPriceFactorsSum = 6.0;

  Market.prototype.buyFromMarketPriceFactors = {
    constantBuyMargin: 1.0,
    transactionAmount: 0.5,
    localBusinesses: 1.0,
    randomEvents: 1.0,
    additionalFeatures: 0.5,
    availableAmounts: 1.0
  };  // lower better; buy price = basePrice + basePrice * factors / 5; buy from market range: (basePrice, 2 * basePrice)
  Market.prototype.buyFromMarketPriceFactorsSum = 5.0;

  Market.prototype.yourMarketShares = null;
  Market.prototype.yourRecentAmountsSoldToMarket = null;
  Market.prototype.otherRecentAmountsSoldToMarket = null;
  Market.prototype.transactionMemory = 600;  // ticks
  Market.prototype.nextAmountsSetup = 0;

  Market.prototype.tick = function () {
    this.checkAmountsSetup();
  };

  Market.prototype.checkAmountsSetup = function () {
    if (this.nextAmountsSetup > 0) {
      this.nextAmountsSetup--;
    } else {
      this.nextAmountsSetup = Math.ceil(Math.random() * 120 + 60);
      this.setupAmounts();
    }
  };

  Market.prototype.setupAmounts = function () {
    this.tradableResources.forEach(function (resource) {
      //TODO: localBusinesses influence
      resource.amountsAvailableOnLocalMarket = [
        Math.ceil(Math.random() * resource.baseLocalMarketAmount),
        Math.ceil(Math.random() * 10 * resource.baseLocalMarketAmount + resource.baseLocalMarketAmount),
        Math.ceil(Math.random() * 100 * resource.baseLocalMarketAmount + 10 * resource.baseLocalMarketAmount)
      ];
      resource.amountsLocalMarketNeeds = [
        Math.ceil(Math.random() * resource.baseLocalMarketAmount / 2),
        Math.ceil(Math.random() * 5 * resource.baseLocalMarketAmount + resource.baseLocalMarketAmount),
        Math.ceil(Math.random() * 50 * resource.baseLocalMarketAmount + 5 * resource.baseLocalMarketAmount)
      ];
    });
  };

  Market.prototype.buyFromMarket = function (resource, amount) {
    var price = this.getBuyPrice(resource, amount);
    var value = price * amount;
    this.recalculatePrices(resource);
  };

  Market.prototype.sellToMarket = function (resource, amount) {
    var price = this.getSellPrice(resource, amount);
    var value = price * amount;
    this.recalculatePrices(resource);
  };

  Market.prototype.recalculatePrices = function (resource) {
    resource.buyPrice = this.getBuyPrice(resource, 0);
    resource.sellPrice = this.getSellPrice(resource, 0);
  };

  Market.prototype.setupInitialPrices = function () {
    var that = this;
    this.tradableResources.forEach(function (resource) {
      that.recalculatePrices(resource);
    });
  };

  Market.prototype.getBuyPrice = function (resource, amount) {
    return resource.basePrice +
      resource.basePrice * this.calculateBuyPriceFactors(resource, amount) / this.buyFromMarketPriceFactorsSum;
  };

  Market.prototype.getSellPrice = function (resource, amount) {
    return resource.basePrice * this.calculateSellPriceFactors(resource, amount) / this.sellToMarketPriceFactorsSum;
  };

  Market.prototype.calculateBuyPriceFactors = function (resource, amount) {
    return this.buyFromMarketPriceFactors.constantBuyMargin +
      this.calculateAmountFactor(amount, this.buyFromMarketPriceFactors.transactionAmount) +
      this.calculateAvailableAmountsFactor(resource) +
      this.calculateLocalBusinessesBuyFactor(resource) +
      this.calculateRandomEventsBuyFactor(resource) +
      this.calculateAdditionalFeaturesBuyFactor(resource);
  };

  Market.prototype.calculateSellPriceFactors = function (resource, amount) {
    var now = tickerProvider.now();
    return this.calculateYourMarketShareFactor(now, resource) +
      this.calculateAmountFactor(amount, this.sellToMarketPriceFactors.transactionAmount) +
      this.calculateNeededAmountsFactor(resource) +
      this.calculateLocalBusinessesSellFactor(resource) +
      this.calculateRandomEventsSellFactor(resource) +
      this.calculateAdditionalFeaturesSellFactor(resource);
  };

  Market.prototype.calculateYourMarketShareFactor = function (now, resource) {
    var factorBaseImpact = this.sellToMarketPriceFactors.yourMarketShare;
    var yourAmountSold = this.getAmountSoldTimed(now, this.yourRecentAmountsSoldToMarket[resource.resName]);
    var otherAmountSold = this.getAmountSoldTimed(now, this.otherRecentAmountsSoldToMarket[resource.resName]);
    var yourMarketShare = (1 + yourAmountSold) / (1 + yourAmountSold + otherAmountSold);
    return factorBaseImpact * yourMarketShare;
  };

  Market.prototype.getAmountSoldTimed = function (now, amountsSold) {
    var totalAmountSoldTimed = 0;
    var outdatedTransactions = [];
    for (var soldTickStamp in amountsSold) {
      var amountSold = amountsSold[soldTickStamp];
      var tickDiff = now - soldTickStamp;
      if (tickDiff > this.transactionMemory) {
        outdatedTransactions.push(soldTickStamp);
      } else {
        totalAmountSoldTimed += (1 - tickDiff / this.transactionMemory) * amountSold;
      }
    }
    outdatedTransactions.forEach(function (soldTickStamp) {
      delete amountsSold[soldTickStamp];
    });
    return totalAmountSoldTimed;
  };

  Market.prototype.calculateAmountFactor = function (amount, factorBase) {
    if (amount <= 1) {
      return 0;
    }
    var amountFactor = amount > 100000 ? 1 : Math.log10(amount) / 5.0;
    return factorBase * amountFactor;
  };

  Market.prototype.calculateLocalBusinessesBuyFactor = function (resource) {
    return this.buyFromMarketPriceFactors.localBusinesses;
  };

  Market.prototype.calculateLocalBusinessesSellFactor = function (resource) {
    return this.sellToMarketPriceFactors.localBusinesses;
  };

  Market.prototype.calculateRandomEventsBuyFactor = function (resource) {
    return this.buyFromMarketPriceFactors.randomEvents;
  };

  Market.prototype.calculateRandomEventsSellFactor = function (resource) {
    return this.sellToMarketPriceFactors.randomEvents;
  };

  Market.prototype.calculateAdditionalFeaturesBuyFactor = function (resource) {
    return this.buyFromMarketPriceFactors.additionalFeatures;
  };

  Market.prototype.calculateAdditionalFeaturesSellFactor = function (resource) {
    return this.sellToMarketPriceFactors.additionalFeatures;
  };

  Market.prototype.calculateAvailableAmountsFactor = function (resource) {
    return this.buyFromMarketPriceFactors.availableAmounts;
  };

  Market.prototype.calculateNeededAmountsFactor = function (resource) {
    return this.sellToMarketPriceFactors.neededAmounts;
  };

  Market.prototype.setupInitialRecentAmountsSold = function () {
    this.yourRecentAmountsSoldToMarket = {};
    this.otherRecentAmountsSoldToMarket = {};
    var that = this;
    this.tradableResources.forEach(function (resource) {
      that.yourRecentAmountsSoldToMarket[resource.resName] = {};
      that.otherRecentAmountsSoldToMarket[resource.resName] = {};
    });
  };

  Market.prototype.setupInitialMarketShares = function () {
    this.yourMarketShares = {};
    var that = this;
    this.tradableResources.forEach(function (resource) {
      that.yourMarketShares[resource.resName] = 0.0;
    });
  };

  return Market;
}]);