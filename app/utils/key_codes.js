'use strict';

angular.module('digger').factory('keyCodes', function () {

  var codeToNum = {
    backspace: 8,
    tab: 9,
    enter: 13,
    shift: 16,
    ctrl: 17,
    alt: 18,
    pause: 19,
    capslock: 20,
    esc: 27,
    space: 32,
    pageup: 33,
    pagedown: 34,
    end: 35,
    home: 36,
    left: 37,
    up: 38,
    right: 39,
    down: 40,
    insert: 45,
    del: 46,
    command: 91,
    leftcommand: 91,
    rightcommand: 93,
    numast: 106,
    numplus: 107,
    numminus: 109,
    numdot: 110,
    numslash: 111,
    numlock: 144,
    scrolllock: 145,
    mycomputer: 182,
    mycalculator: 183,
    semicol: 186,
    equal: 187,
    coma: 188,
    minus: 189,
    dot: 190,
    slash: 191,
    sqbracketopen: 219,
    backslash: 220,
    sqbracketclose: 221,
    apostrophe: 222
  };

  // numbers
  for (var i = 48; i < 58; i++) {
    codeToNum[i - 48] = i;
  }

  // lower case chars
  for (i = 97; i < 123; i++) {
    codeToNum[String.fromCharCode(i)] = i - 32;
  }

  // function keys
  for (i = 1; i < 13; i++) {
    codeToNum['f'+i] = i + 111;
  }

  // numpad numbers
  for (i = 0; i < 10; i++) {
    codeToNum['num'+i] = i + 96
  }

  // Reverse mapping
  var numToCode = {};
  for (var code in codeToNum) {
    numToCode[codeToNum[code]] = code;
  }

  return {
    // Return descriptive code
    toCode: function (num) {
      return numToCode[num];
    },
    // Return key number
    fromCode: function (code) {
      return codeToNum[code];
    },
    _instance: this
  };
});
